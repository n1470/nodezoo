# Development with AWS

In following commands, change profile name as needed or remove if using default profile for AWS CLI and tools.

## Create a cluster manually
Use this method to understand EKS cluster creation almost step-by-step.

```shell
eksctl create cluster \
    --profile cdk \
    --name nodezoo-dev \
    --tags "project-env=nodezoo-dev" \
    --region ap-south-1 \
    --version 1.22 \
    --with-oidc \
    --node-type t3a.medium \
    --nodes 3 \
    --nodes-min 3 \
    --nodes-max 5 \
    --node-volume-size 8 \
    --node-private-networking \
    --managed \
    --full-ecr-access \
    --alb-ingress-access \
    --set-kubeconfig-context
```

### Add the Addons

```shell
for ao in kube-proxy vpc-cni coredns                                 
do               
  eksctl create addon --profile cdk --cluster nodezoo-dev --name $ao
done
```

### (Optional) Add the Web Console User to K8s Auth

```shell
eksctl create iamidentitymapping \
    --profile cdk \
    --cluster nodezoo-dev \
    --region ap-south-1 \
    --arn arn:aws:iam::${AWS_ACCOUNT_ID}:user/${WEB_CONSOLE_USER} \
    --group system:masters \
    --no-duplicate-arns
```

### Add ALB Controller

```shell
# download iam_policy to allow the load balancer to make API calls on our behalf
curl -o /tmp/iam_policy.json \
https://raw.githubusercontent.com/kubernetes-sigs/aws-load-balancer-controller/v2.4.1/docs/install/iam_policy.json
```

```shell
aws --profile cdk iam create-policy \
    --policy-name AWSLoadBalancerControllerIAMPolicy \
    --policy-document file:///tmp/iam_policy.json
```

```shell
eksctl create iamserviceaccount \
  --profile cdk \
  --cluster=nodezoo-dev \
  --namespace=kube-system \
  --name=aws-load-balancer-controller \
  --role-name "AmazonEKSLoadBalancerControllerRole" \
  --attach-policy-arn=arn:aws:iam::${AWS_ACCOUNT_ID}:policy/AWSLoadBalancerControllerIAMPolicy \
  --approve
```

```shell
kubectl apply \
    --validate=false \
    -f https://github.com/jetstack/cert-manager/releases/download/v1.5.4/cert-manager.yaml

curl -Lo /tmp/v2_4_1_full.yaml \
https://github.com/kubernetes-sigs/aws-load-balancer-controller/releases/download/v2.4.1/v2_4_1_full.yaml

sed -i.bak -e 's|your-cluster-name|nodezoo-dev|' /tmp/v2_4_1_full.yaml
sed -i.bak -e "s|your-vpc-id|${VPC_ID}|" /tmp/v2_4_1_full.yaml
sed -i.bak -e 's|your-region|ap-south-1|' /tmp/v2_4_1_full.yaml
# Remove kind: ServiceAccount section and save file
kubectl apply -f /tmp/v2_4_1_full.yaml
# verify
kubectl get deployment -n kube-system aws-load-balancer-controller
```

The cluster is now ready for adding services.

## Nodezoo EKS Cluster
Main cluster for the information services.

See `nodezoo-k8s-cluster/README.md` for instructions on creating the cluster.

## Nodezoo API Gateway
Once the info-services cluster is up, check the `nodezoo-api/README.md` for development with AWS.


## Create EC2 instance for testing
Sentinel to access internal load balancer.

### Run the instance
```shell
export IMAGE_ID=$(aws --profile cdk ssm get-parameters-by-path \
--path /aws/service/ami-amazon-linux-latest \
--query "Parameters[].Name" | jq '.[] | match("amzn(\\d)?-ami-hvm-x86_64-ebs") | .string' -r | head -n1)

export VPC_ID=$(aws --profile cdk ec2 describe-vpcs \
--filter 'Name=tag:aws:cloudformation:stack-name,Values=NodezooK8SClusterStack' \
--query 'Vpcs[].VpcId' | jq -r '.[]')

export PUBLIC_SUBNET_ID=$(aws --profile cdk ec2 describe-subnets \
--filter "Name=vpc-id,Values=${VPC_ID}" \
--filter 'Name=tag:aws:cloudformation:stack-name,Values=NodezooK8SClusterStack' \
--filter 'Name=tag:aws-cdk:subnet-type,Values=Public' | jq -r '.Subnets[].SubnetId' | head -n1)

export SECURITY_GROUP_ID=$(aws --profile cdk ec2 create-security-group \
--vpc-id ${VPC_ID} \
--group-name WorldSSh \
--description 'Allows access from anywhere' | jq -r '.GroupId')

aws --profile cdk ec2 authorize-security-group-ingress \
--group-id ${SECURITY_GROUP_ID} \
--ip-permissions 'FromPort=22,IpProtocol=tcp,IpRanges=[{CidrIp=0.0.0.0/0}],ToPort=22'

export INSTANCE_ID=$(aws --profile cdk ec2 run-instances \
--image-id resolve:ssm:/aws/service/ami-amazon-linux-latest/${IMAGE_ID} \
--instance-type t3a.small \
--key-name general_purpose \
--security-group-ids ${SECURITY_GROUP_ID} \
--subnet-id ${PUBLIC_SUBNET_ID} \
--count 1 \
--associate-public-ip-address \
--block-device-mappings 'DeviceName=/dev/sda1,Ebs={DeleteOnTermination=true,VolumeType=gp3,VolumeSize=8}' | \
jq '.Instances[].InstanceId' -r)

# wait till "state" is "running"
while [ -z "$RUNNING_STATE" ]; do
  sleep 5
  aws --profile cdk ec2 describe-instances \
  --instance-ids ${INSTANCE_ID} | \
  jq -r '{host: .Reservations[].Instances[].PublicDnsName, state: .Reservations[].Instances[].State.Name}' | \
  tee /tmp/sentinel-state.json
  RUNNING_STATE=$(jq 'select(.state == "running")' /tmp/sentinel-state.json)
done

# wait till both instance and system status have passed
while [ -z "$PASSED_INSTANCE_STATUS" ]; do
  sleep 5
  aws --profile cdk ec2 describe-instance-status --instance-ids ${INSTANCE_ID} | \
  jq '{InstanceStatus: .InstanceStatuses[].InstanceStatus.Details[].Status, SystemStatus: .InstanceStatuses[].SystemStatus.Details[].Status}' | \
  tee /tmp/sentinel-instance-state.json
  PASSED_INSTANCE_STATUS=$(jq 'select(.InstanceStatus == "passed" and .SystemStatus == "passed")' /tmp/sentinel-instance-state.json)  
done

export EC2_INSTANCE_HOST=$(jq -r '.host' /tmp/sentinel-state.json)
```

### Find Info Load Balancer
```shell
export LB_HOST=$(aws --profile cdk elbv2 describe-load-balancers | \
jq -r '.LoadBalancers[] | select(.LoadBalancerName | test("k8s-nodezoo-info")) | .DNSName')
```

### Connect to the instance
```shell
ssh ec2-user@${EC2_INSTANCE_HOST}
```
Note that the SSH user is `ec2-user`.```

### Test the info service
Substitute the value for `${LB_HOST}`
```shell
$ curl -v http://${LB_HOST}/info?q=express
```

or try
```shell
ssh ec2-user@${EC2_INSTANCE_HOST} -c curl -v http://${LB_HOST}/info?q=express
```

## Cleaning up

```shell
aws --profile cdk ec2 terminate-instances --instance-ids ${INSTANCE_ID}

aws --profile cdk ec2 describe-instances \
--instance-ids ${INSTANCE_ID} | \
jq -r '{host: .Reservations[].Instances[].PublicDnsName, state: .Reservations[].Instances[].State.Name}'
# wait till state is "terminated"

aws --profile cdk ec2 delete-security-group --group-id ${SECURITY_GROUP_ID}
```

```shell
AWS_PROFILE=cdk make delete_stack
```
This target will also remove the load balancers. Overall step may take several minutes. This step may also not 
be able to delete all resources; a manual check is needed if the reported status is FAILED at the end.
