# Node Zoo

``Node Zoo`` is a microservice reference implementation of the Nodezoo case study from the [Tao of Microservices - Richard Roger](https://www.manning.com/books/the-tao-of-microservices).

## Architecture
![Nodezoo Architecture](https://viewer.diagrams.net/?edit=_blank#Uhttps%3A%2F%2Fgitlab.com%2Fn1470%2Fnodezoo%2F-%2Fraw%2Fmain%2FMicroservices%2520Technical%2520Preview.drawio)

## EKS Development
See `eks-development.md`.

## Nodezoo EKS Cluster
An EKS cluster runs the information module services:

- `nodezoo-info-service`
- `nodezoo-npm-service`
- `nodezoo-github-service`

### Install

1. Create a master role. Make a note of the `role_arn`.
2. Clone the `nodezoo-k8s-cluster` repo.
    1. Run `make bootstrap` to bootstrap the CDK. This is needed only once. This step will create a CF stack titled
       `CDKToolkit`. In the next step, each make target can be prefixed with `ci_` to run the process in the CI container. The CI container needs to be built prior to this step.
    1. Export the master role as an environment variable with name `MASTER_ROLE_ARN`. 
    1. Run `make deploy`. This step will create the EKS cluster.
    1. Once the cluster is created, make a note of the `kubectl` config command generated as an Cfn output. Execute the command to provide `kubectl` the current EKS context.
    1. Create the `nodezoo` namespace: `kubectl create ns nodezoo`.


### Deploy
1. Clone the `nodezoo-flux` repo. Run `make` to install Flux CD, sources and apps in the EKS cluster. Flux CD will deploy the service manifests from `nodezoo-k8s-manifests`.

## Nodezoo API
Installation and deployment is a single step.

1. Clone `nodezoo-api` repository and run `make dist deploy`. This target will create the API gateway, the AWS Lmabdas and SQS queues.
2. Make a note of the API gateway as a Cfn output at the end of the previous step.

## Usage
Look for an NPM package like `lodash`. Substitute the API gateway host and package-name in the command below.
```shell
curl -v http://<api-gateway>/info?package=<package-name>
```

## Support
If you face issues or have ideas for future releases, you can add them to the project issue tracker.


## License
MIT for the most case. See individual systems in `Nodezoo` group for specifics.